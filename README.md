# Playbooks to bootstrap your NVNetPerf setup

## Quick Start

- Clone this repo to a host with Ansible 2.12.x or later / or follow the Automation Container instructions below:
   - `git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_bootstrap.git`
   -  `cd doca_perftest_bootstrap`
- Edit the `hosts` file to set hosts address, username and password.
- Run playbooks (e.g.`ansible-playbook nvnetperf-install-doca-zt.yml`).

## Installation Server
The installation playbooks should be run on a separate server/container.
The servers hosting the DPUs, are power-cycled multiple times by the installation playbook and are not suitable as the installation server.

## Automation Container
To use ansible you can install the ansible packages on the test server by following these [instructions](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Alternately you can use a pre-built `automation` container that have the relevant ansible packages already installed. The next few steps will outline how to install the `ansible automation` container -

1. Docker (Linux, Mac, Windows) - Follow this [link](https://docs.docker.com/engine/install/) to the docker install instructions for your platform

2. Pull the automation container from Docker Hub with the following command:
   `sudo docker pull ipspace/automation:ubuntu`

3. Run the container with following command:
   `sudo docker run -it -d ipspace/automation:ubuntu`

4. Next, log into the container with the following command:
   `sudo docker exec -it $(sudo docker ps | grep -i automation | awk -F" " '{print $1}') bash`

   You will see the prompt change to something similar to the following:
   `root@032f1ada86f4:/ansible#`


## Running the playbooks

If you are using an automation container, run these within the container.

1. Clone the NVNetPerf bootstrap repo with the following command:
   `git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_bootstrap.git`

2. Change directory:
   `cd doca_perftest_bootstrap`

3. Use vim or nano to edit the "hosts" file in this directory

   -  Change the following settings for the hosts:
      ```
      host_1 ansible_host=<host-ip-address> pci_addresses='["<dpu-pcie-address>", "<dpu-pcie-address>"]'
      host_2 ansible_host=<host-ip-address> pci_addresses='["<dpu-pcie-address>"]'
      ```
      Update the correct credentials under the `[hosts:vars]` heading:
      ```
      ansible_user=<host-username>
      ansible_password=<host-password>
      ```
      Can also be specified on the host line if they are not the default.

   -  Change the following settings for the DPUs:
      ```
      dpu_1 ansible_host=<dpu-ip-address>
      dpu_2 ansible_host=<dpu-ip-address>
      ```
      Update the correct credentials under the `[dpus:vars]` heading:
      ```
      ansible_user=<dpu-username>
      ansible_password=<dpu-password>
      ```
      can also be specified on the dpu line if they are not the default.

   -  If `Zero Trust` mode is required, Change the following settings for the DPU-BMCs.
      On a factory-default card, the DPU BMC password, specified in the inventory file, is programmed by the installation playbook. The DPU BMC password specified here should meet the following criteria -
      - Minimum length: 13
      - Minimum upper case characters: 1
      - Minimum lower case characters: 1
      - Minimum digits: 1

      ```
      dpu_bmc_1 ansible_host=<dpu-bmc-ip-address>
      dpu_bmc_2 ansible_host=<dpu-bmc-ip-address>
      ```
      Update the correct credentials under the `[dpu_bmcs:vars]` heading:
      ```
      ansible_user=<dpu-bmc-username>
      ansible_password=<dpu-bcm-password>
      ```
      Add a path to the BFB from the test server, under the `[dpu_bmcs:vars]` heading, if installation is required:
      ```
      bfb="/path/to/bfb/from/test/server"
      ```
      can also be specified on the dpu_bmc line if they are not the default

4. Test out the Ansible inventory file with the following command:

   ```
   ansible hosts -m ping --become
   ```

   This should produce an output similar to the following:

   ```
   host_1 | SUCCESS => {
      "ansible_facts": {
         "discovered_interpreter_python": "/usr/bin/python"
      },
      "changed": false,
      "ping": "pong"
   }

5. Run the appropriate playbook as outlined in the rest of this README file. If a play fails you can use specify `-v` in the command line to get additional debug information for e.g. `ansible-playbook <playbook> -v`.

6. To execute the run command as a non-root user on the test-server, prepend `sudo` to the command, for e.g. `sudo ansible-playbook <playbook>`.

## DPU modes
These playbooks allow you install the DPU OS in the `Zero Trust mode` or `Host Trusted mode`.

### Host Trusted Mode
- DPU is in the default ECPF/privileged mode.
- Host can ssh into the DPU using the host-rshim network, change NIC config and program the DPU's acceleration engines.
- To install the DPU OS in this mode from BMC, run `ansible-playbook nvnetperf-install-doca-from-BMC.yml -e dpu_mode="DPU"`.
- To install the DPU OS in this mode from rshim, run `ansible-playbook nvnetperf-install-doca.yml -e dpu_mode="DPU"`.

### Zero Trust Mode
- DPU access is restricted and only possible via the DPU BMC and DPU OOB/1G port.
- In this mode the host cannot access the DPU via the host-rshim network. The host cannot change the NIC config or program the network accelerators.
- To install the DPU OS in this mode run `ansible-playbook nvnetperf-install-doca-zt.yml`.

## Playbooks' Description
### `nvnetperf-install-doca.yml`

<img src=./docs/host-trusted.jpg alt="Host Trusted" width=60% height=60% title="Host Title">

This is the playbook to get hosts and DPUs fully ready to run DOCA applications. This playbook can only be used in the DPU default/ECPF/privileged mode where the host is trusted.

- Installs DOCA software on the hosts. The latest release is installed by default. You can optionally specify the DOCA either by specifying the path with `-e doca_for_host="path/to/your/doca"` or by providing a URL for the installation (ensure the link starts with "http://" or "https://").
- Installs the DPU BFB image from the host-rshim network. You must provide the BFB image either by specifying the path with `-e bfb="path/to/your/bfb"` or by providing a URL for the installation (ensure the link starts with "http://" or "https://").
- Puts the DPUs in the required mode if `"dpu_mode"` is provided (e.g., `-e dpu_mode="ZT"`); otherwise, it will remains with the default mode.
- Updates the DPU firmware as a part of the DPU BFB install.
- Sets up the Rshim network to allow ssh from the host to the DPU. After the install you can access the DPU Arm sub-system from the host by running `ssh rshim0`. If you have multiple cards use the rshim-id associated with the card (e.g. `ssh rshim1`), etc.
- Changes the link type from IB to Ethernet on VPI cards. You can optionally specify the required link type by using `-e link_type="IB"` or `-e link_type="ETH"`.
- Enables `Service Function Chaining` required by `Host Based Networking` if `hbn_enable` is set to true (HBN is disabled by default).
- Installs utilities packages on the hosts and DPUs that relevant to the `project`.

The hosts are rebooted multiple times during the install.

Arguments/ansible-variables are passed with the `-e` flag.

You can optionally specify the project as a command line argument, `-e project=nvqual` or `-e project=nvnetperf`. The default project is nvnetperf.

Install with HBN disabled -
```
ansible-playbook nvnetperf-install-doca.yml -e bfb="path/to/your/bfb"
```
Install with HBN enabled -
```
ansible-playbook nvnetperf-install-doca.yml -e bfb="path/to/your/bfb" -e hbn_enable=true
```

Note: To complete the downloads, certain Python packages need to be installed on the test server to enable it to run the tests. Please ensure this is done in the 'Requirements.txt file' section of the [NVNetPerf Tests README](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests).

### `nvnetperf-config-nic-mode.yml`

This is the playbook to get your DPUs to be in NIC mode. To use this playbook you need to specify the DPU BMCs and DPUs OOB IPv4 addresses in the inventory file (hosts). The DPU BMCs needs be in the factory default state or it needs to be accessible via the password provided in the inventory file.

This playbook -
- Change the DPU BMCs default password `0penBmc`.
- Configure the DPUs to be in NIC mode.
The hosts are rebooted for the DPUs changes to take effect.
```
ansible-playbook nvnetperf-config-nic-mode.yml
```

### `nvnetperf-config-zt-mode.yml`

This is the playbook to get your DPUs to be in ZT mode (restricted mode).
Assume it's not in NIC mode. if so, should clean-up NIC mode before the usage, with the dedicated playbook.
The hosts are rebooted for the DPUs changes to take effect.
```
ansible-playbook nvnetperf-config-zt-mode.yml
```

### `nvnetperf-dpu-mode-config-from-BMC.yml`
This playbook configures the DPUs to `ZT` for nvnetperf and `DPU` for nvqual (using -e project=nvqual).
The required mode can be specified as a command-line argument using `-e dpu_mode="ZT"`, `-e dpu_mode="DPU"` or `-e dpu_mode="NIC"`.
Assume it's not in ZT. if so, should clean-up ZT before the usage, with the dedicated playbook.
```
ansible-playbook nvnetperf-dpu-mode-config-from-BMC.yml
```

### `nvnetperf-config-dpu-bmc-password.yml`
This playbook can be used to configure the DPU-BMCs password on a factory-default card -
- The play assumes that the DPU BMCs is accessible via the default, one-time password `0penBmc`
- This default password is changed to the DPU BMCs password specified in the inventory file.

The DPU BMCs is only used in the Zero-Trust mode. Running this playbook is optional in Zero-Trust mode. Alternately you can run the `nvnetperf-install-doca-zt.yml` play directly to change the password and install the DPU OS.

### `nvnetperf-install-doca-zt.yml`

<img src=./docs/zero-trust.jpg alt="Zero Trust" width=70% height=70% title="Zero Title">

This is the playbook to get hosts and DPUs fully ready to run DOCA applications.
To use this playbook you need to specify the DPU BMCs and DPU OOB IPv4 addresses in the inventory file (hosts).
The DPU BMCs needs be in the factory default state or it needs to be accessible via the password provided in the inventory file.

The playbook -
- Programs the DPU BMCs password on a `factory default` card. The default/one-time, DPU BMCs password on a factory-default card is `0penBmc`. If the card is in a factory-default state the password is changed to the DPU-BMCs password provided in the inventory file.
- Installs DOCA software on the hosts. The latest release is installed by default. You can optionally specify the DOCA either by specifying the path with `-e doca_for_host="path/to/your/doca"` or by providing a URL for the installation (ensure the link starts with "http://" or "https://").
- Installs the DPU BFB image from the DPU BMC. You must provide the BFB image either by specifying the path or by providing a URL for the installation (ensure the link starts with "http://" or "https://").
It can be provided with `-e bfb="/path/to/bfb/from/test/server"` or in the inventory file under the `[dpu_bmcs:vars]` heading.
For multiple DPUs, if you need to install different BFB image for each card, specify the BFB image for each card as a dpu_bmc variable in the inventory file.
To define a default BFB value, specify it under `[dpu_bmcs:vars]`.
- Updates the DPU firmware as a part of the DPU BFB install.
- Changes the link type from IB to Ethernet on VPI cards. You can optionally specify the required link type by using `-e link_type="IB"` or `-e link_type="ETH"`.
- Enables `Service Function Chaining` required by `Host Based Networking` if `hbn_enable` is set to true (HBN is disabled by default).
- Puts the DPUs in Zero-Trust mode (restricted mode).
- Installs utilities packages on the hosts and DPUs that relevant to the `project`. In case the project is nvnetperf, it will also install packages related to GPU-Direct-RDMA test, if you wish to skip this use: `--skip-tags "install_gdr_utils"`.

The hosts are rebooted multiple times during the install.

You can optionally specify the project as a command line argument, `-e project=nvqual` or `-e project=nvnetperf`. The default project is nvnetperf.

Install with HBN disabled -
```
ansible-playbook nvnetperf-install-doca-zt.yml
```
Install with HBN enabled -
```
ansible-playbook nvnetperf-install-doca-zt.yml -e hbn_enable=true
```

The playbook installs the latest release BFB version by default. You can also see `group_vars/all/main.yml` for the image URLs and versions.

Note: To complete the downloads, certain Python packages need to be installed on the test server to enable it to run the tests. Please ensure this is done in the 'Requirements.txt file' section of the [NVNetPerf Tests README](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests).

### `nvnetperf-install-doca-from-BMC.yml`

This playbook, derived from `nvnetperf-install-doca-zt.yml`, follows the same installation steps.
However, it differs in that it will set the DPUs to the default mode according to the project:
`ZT` for `nvcert` and `DPU` for `nvqual`.
Alternatively, You can specify the desired mode via command-line arguments using `-e dpu_mode="ZT"`, `-e dpu_mode="DPU"`, or `-e dpu_mode="NIC"`.
```
ansible-playbook nvnetperf-install-doca-from-BMC.yml
```

### `nvnetperf-check-sn.yml`
This playbook can be used to pull and cross-check `Serial Number` from the DPU-BMCs, DPUs and server.

Running the playbook is optional. It is typically used to validate the inventory file.

NOTE: This playbook supports only a single server in the hosts file.

```
ansible-playbook nvnetperf-check-sn.yml
```

### `nvnetperf-install-hbn-container.yml`

Deploys the DOCA HBN (Host Based Networking) service container on the DPUs. DOCA HBN provides classic top of rack (ToR) routing/switching/network overlay capabilities on the DPUs for the host.

You can install either the latest release or a development container -
1. By default the latest release container is installed.
2. If you need to install the latest development container instead, specify your NGC API key via `-e ngc_api_key=<your-key>`.

You can also see `group_vars/all/main.yml` for the container NGC URLs and versions.
```
ansible-playbook nvnetperf-install-hbn-container.yml
```

### `nvnetperf-hbn-query.yml`

Pull info about the DOCA HBN (Host Based Networking) service container on the DPUs.

```
ansible-playbook nvnetperf-hbn-query.yml
```

### `nvnetperf-cleanup-zt.yml`

If you onboarded the DPUs for certification in Zero Trust mode you need to reset it to factory defaults using this playbook, after certification testing, before returning the DPUs to the common pool. This is a `mandatory step` to ensure that the DPU can be installed and provisioned by subsequent users as described in the BlueField User Guide.

This playbook -
- Returns the DPUs to the default embedded mode where the host is trusted.
- Resets the DPU BMCs password to the factory default - `0penBmc` and cold resets the BMC.
The hosts are rebooted for the DPUs changes to take effect.
```
ansible-playbook nvnetperf-cleanup-zt.yml
```

### `nvnetperf-cleanup-nic-mode.yml`

If you onboarded the DPUs for certification in NIC mode you need to reset it to factory defaults using this playbook, after certification testing, before returning the DPUs to the common pool. This is a `mandatory step` to ensure that the DPU can be installed and provisioned by subsequent users as described in the BlueField User Guide.

This playbook -
- Returns the DPUs to the default DPU mode.
- Resets the DPU BMCs password to the factory default - `0penBmc` and cold resets the BMC
The hosts are rebooted for the DPUs changes to take effect.
```
ansible-playbook nvnetperf-cleanup-nic-mode.yml
```

## Additional Documentation
- [DOCA release Notes](https://docs.nvidia.com/doca/sdk/index.html#src-2571330351_DOCADocumentationv2.6.0-DOCADocumentationv2.6.0)

## Authors
- Hala Awisat <hawisat@nvidia.com>
- Anuradha Karuppiah <anuradhak@nvidia.com>
- Mike Courtney <mcourtney@nvidia.com>
- Justin Betz <jubetz@nvidia.com>
