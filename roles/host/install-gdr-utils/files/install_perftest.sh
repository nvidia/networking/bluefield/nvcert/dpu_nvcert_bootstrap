#!/bin/bash

set -e

DIR="/tmp/perftest"

if [ -d "$DIR" ]; then
    sudo rm -rf "$DIR"
fi

sudo modprobe nvidia_peermem
cuda_version=`sudo jq '.cuda.version' /usr/local/cuda/version.json | cut -d'.' -f1,2 | cut -d'"' -f2`
# cuda_version=`sudo nvidia-smi | grep "CUDA Version" | awk '{print $9}'`
if [ -z "$cuda_version" ]; then
    echo "No CUDA avaiable"
    exit
fi
cd /tmp/
sudo git clone https://github.com/linux-rdma/perftest.git
cd perftest
export LD_LIBRARY_PATH="/usr/local/cuda-${cuda_version}/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
export CUDA_H_PATH="/usr/local/cuda-${cuda_version}/include/cuda.h"
sudo ./autogen.sh
sudo ./configure CUDA_H_PATH="/usr/local/cuda-${cuda_version}/include/cuda.h" --prefix=/usr --sbindir=/usr/bin --libexecdir=/usr/lib --sysconfdir=/etc --localstatedir=/var --mandir=/usr/share/man
sudo make -j "$(($(nproc) + 1))"
sudo make install