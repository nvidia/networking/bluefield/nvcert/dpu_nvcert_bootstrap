##
## SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
## Copyright (c) 2023-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
## SPDX-License-Identifier: LicenseRef-NvidiaProprietary
##
## NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
## property and proprietary rights in and to this material, related
## documentation and any modifications thereto. Any use, reproduction,
## disclosure or distribution of this material and related documentation
## without an express license agreement from NVIDIA CORPORATION or
## its affiliates is strictly prohibited.
##

### This role is currently not in use

# These tasks are intended to be run on the test server. They update the DPU's
# BMC version to latest

- name: Setup working directory on local host
  ansible.builtin.set_fact:
    local_dir: "/tmp/nvnetperf_bootstrap"
  delegate_to: localhost

- name: Setup BMC download path
  ansible.builtin.set_fact:
    bmc_download_path: "{{ bmc_download_url }}/{{ bmc_image }}"
  delegate_to: localhost

- name: Checking if BMC has been downloaded
  stat:
    path: "{{ local_dir }}/{{ bmc_image }}"
  register: bmcdownload
  delegate_to: localhost

#TODO: adding option to provide local bmc image
- name: Download BMC image
  get_url:
    url: "{{ bmc_download_path }}"
    dest: "{{ local_dir }}"
  delegate_to: localhost
  when: not bmcdownload.stat.exists

- name: Print message to user
  ansible.builtin.debug:
    msg: Starting BMC update, this may take up to 15 minutes
  delegate_to: localhost

#TODO: move to general.redfish_command
- name: Update BMC version
  command: /usr/bin/env python3
  args:
    stdin: |
        import os
        import time
        import subprocess
        import json
        tmp_bmc_host = "{{ ansible_host }}"
        tmp_bmc_password = "{{ ansible_password }}"
        update_cmd = "curl -k -u root:%s -H 'Content-Type:application/octet-stream' -X POST -T {{ local_dir }}/{{ bmc_image }} https://%s/redfish/v1/UpdateService" % (tmp_bmc_password, tmp_bmc_host)
        print(update_cmd)
        update_cmd_output = subprocess.check_output(update_cmd.split())
        try:
            update_cmd_output = json.loads(update_cmd_output)
        except:
            print("Redfish command Failed")
            exit(1)
        task_id = update_cmd_output.get("Id")
        cmd = f"curl -k -u root:%s -X GET https://%s/redfish/v1/TaskService/Tasks/%s" % (tmp_bmc_password, tmp_bmc_host, task_id)
        print(cmd)
        task_state = "Running"
        while(task_state == "Running"):
            print("Waiting for BMC upgrade task to complete")
            time.sleep(60)
            output = subprocess.check_output(cmd.split())
            try:
                output = json.loads(output)
            except:
                print("Redfish get task status Failed")
                exit(1)
            task_state = output.get("TaskState")
            if task_state:
                print(f"Task BMC Upgrade status: {task_state.strip()}")
            else:
                message = "Redfish call failed"
                error_log = output.get("error")
                if error_log:
                    message = error_log.get("message")
                    print(message)
                    exit(1)
  delegate_to: localhost

- name: Reboot DPU BMC
  import_role:
    name: dpu_bmc/dpu-bmc-reboot
