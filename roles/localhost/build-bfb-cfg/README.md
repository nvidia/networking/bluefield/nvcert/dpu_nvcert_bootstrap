# Build the configuration file for the BFB install

This role creates the bf.cfg file using a Jinja2 template

## Dependencies

This role is used in conjunction with the "install_bfb" role

## Defaults

`bwfwupdate` is set to true by default, this resutss in the firmware being upgraded and the DPU being reset to defaults.

The above will happen with a single reboot, rather than doing it multiple reboots, as has been the case in the past.
