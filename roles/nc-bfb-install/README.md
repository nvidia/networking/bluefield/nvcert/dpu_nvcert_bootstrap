# Install Bluefield Bitstream Image

This role will download and install a new BFB image to a DPU via the host RSHIM interface.

## Dependencies

- Internet access to download the BFB file.
- RSHIM accessible and installed on the host.
